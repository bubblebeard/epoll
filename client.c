#include<stdio.h> //printf
#include<string.h>    //strlen
#include<sys/socket.h>    //socket
#include<arpa/inet.h> //inet_addr

int main(int argc, char *argv[]) {
    int sock;
    struct sockaddr_in server;
    char message[] = "Hello, server\0", server_reply[2000];

    if (argc != 3) /* Test for correct number of arguments */ {
        fprintf(stderr, "Usage: %s <Server IP> <Echo Port>\n", argv[0]);
        exit(1);
    }

    //Create socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        printf("Could not create socket\n");
    }
    printf("Socket created\n");

    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_family = AF_INET;
    server.sin_port = htons(atoi(argv[2]));

    //Connect to remote server
    if (connect(sock, (struct sockaddr *) &server, sizeof (server)) < 0) {
        perror("connect failed. Error\n");
        return 1;
    }

    printf("Connected\n");

    //keep communicating with server
    while (1) {
        //Send some data
        if (send(sock, message, strlen(message), 0) < 0) {
            printf("Send failed\n");
            close(sock);
            return 1;
        }
        sleep(3);
        //Receive a reply from the server
        if (recv(sock, server_reply, 20, 0) < 0) {
            printf("recv failed\n");
            break;
        }

        printf("Server reply: %s", server_reply);
    }

    close(sock);
    return 0;
}