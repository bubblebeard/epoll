#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>

#define MAXEVENTS 64

int main(int argc, char *argv[]) {
    int sfd, client_sock, c;
    int s, flags, efd;
    struct epoll_event event, *events;
    struct sockaddr_in server, client;
    char reply[] = "Hello, client\0";
    if (argc != 2) /* Test for correct number of arguments */ {
        fprintf(stderr, "Usage: %s <Echo Port>\n", argv[0]);
        exit(1);
    }

    sfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sfd == -1) {
        printf("Could not create socket\n");
    }
    printf("Sockets created\n");

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(atoi(argv[1]));

    //Bind
    if (bind(sfd, (struct sockaddr *) &server, sizeof (server)) < 0) {
        //print the error message
        perror("bind failed. Error\n");
        return -1;
    }
    printf("bind done\n");

    flags = fcntl(sfd, F_GETFL, 0);
    if (flags == -1) {
        perror("fcntl");
        return -1;
    }
    flags |= O_NONBLOCK;
    fcntl(sfd, F_SETFL, flags);

    //Listen
    listen(sfd, SOMAXCONN);
    printf("Socket is listening\n");
    efd = epoll_create1(0);
    if (efd == -1) {
        perror("epoll_create");
        abort();
    }

    event.data.fd = sfd;
    event.events = EPOLLIN | EPOLLET;
    s = epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &event);
    if (s == -1) {
        perror("epoll_ctl");
        abort();
    }
    events = calloc(MAXEVENTS, sizeof event);
    for (;;) {
        int n;
        n = epoll_wait(efd, events, MAXEVENTS, -1);
        for (int i = 0; i < n; i++) {
            if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) ||
                    (!(events[i].events & EPOLLIN))) {
                fprintf(stderr, "epoll error\n");
                close(events[i].data.fd);
                continue;
            } else if (sfd == events[i].data.fd) {
                c = sizeof (struct sockaddr_in);
                for (;;) {
                    client_sock = accept(sfd, (struct sockaddr *) &client, &c);
                    if (client_sock == -1) {
                        if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                            break;
                        } else {
                            perror("accept");
                            break;
                        }
                    }
                    flags = fcntl(client_sock, F_GETFL, 0);
                    if (flags == -1) {
                        perror("fcntl");
                        abort();
                    }
                    flags |= O_NONBLOCK;
                    fcntl(client_sock, F_SETFL, flags);
                    event.data.fd = client_sock;
                    event.events = EPOLLIN | EPOLLET;
                    s = epoll_ctl(efd, EPOLL_CTL_ADD, client_sock, &event);
                    if (s == -1) {
                        perror("epoll_ctl");
                        abort();
                    }
                }
                continue;
            } else {
                int done = 0;
                printf("Data from client%d: ", events[i].data.fd);
                while(done != 1) {
                    ssize_t count;
                    char buf[512];

                    count = read(events[i].data.fd, buf, sizeof buf);
                    if (count == -1) {
                        /* If errno == EAGAIN, that means we have read all
                           data. So go back to the main loop. */
                        if (errno != EAGAIN) {
                            perror("read");
                            done = 1;
                        }
                        break;
                    } else if (count == 0) {
                        /* End of file. The remote has closed the
                           connection. */
                        done = 1;
                        break;
                    }

                    /* Write the buffer to standard output */
                    printf("%s", buf);
                }
                printf("\n");
                printf("Sending reply to client%d\n", events[i].data.fd);
                if (send(events[i].data.fd, reply, strlen(reply), 0) < 0) {
                    printf("Send failed\n");
                    close(events[i].data.fd);
                }
            else 
                if (done) {
                    printf("Closed connection on descriptor %d\n",
                           events[i].data.fd);

                    /* Closing the descriptor will make epoll remove it
                       from the set of descriptors which are monitored. */
                    close(events[i].data.fd);
                }
            }
        }
    }
    //Accept and incoming connection

    return 0;
}